<?php
class Fridge{
    private $name;
    private $capacity = 'undef';
    private $products = [];
    private $fullness = '0';
    protected $type = 'Холодильник';
    public function __construct($name, $capacity){
        $this->name = $name;
        $this->capacity = $capacity;
    }
    public function discription(){
      	echo '<br>';
        echo 'Модель: ';
        echo $this->name;
        echo '<br>';
        echo 'Тип: ';
        echo $this->type;
        echo '<br>';
        echo 'Вместимость камеры: ';
        echo ($this->capacity) - count($this->products);
        echo '<br>';
        echo 'В холодильнике сейчас:';
        if(count($this->products)==0){
            echo 'ничего нет';
        }
        else{
            echo '<br>';
            foreach ($this->products as $product){
                echo $product;
                echo '<br>';
            } 
        }
          
    }   

  	protected function putCompare($product_name){
        $current_fullness = $this->fullness;
        $current_capacity = $this->capacity;
        if($current_fullness+1>$current_capacity){
            return False;
        }else{
            $this->products[] = $product_name;
            $this->fullness+=1;
            return True;
        }
  	}
      public function putProduct($product_name){
        if($this -> putCompare($product_name)==False){
            echo '<br>';
            echo strval($product_name).' не поместился в '.$this->name.'. Холодильник заполнен, вытащите что-нибудь и попробуйте ещё раз';
        }else{
            echo '<br>';
            echo 'Вы добавили '.$product_name.' в холодильник '. $this->name;
        }
    }
    protected function CompareOutProduct($product){
        if(in_array($product, $this->products)){
            unset($this->products[array_search($product,$this->products)]);
            $this->fullness-=1;
            return True;
        }
        else{
            return False;
        }
    }
    public function pullOutProduct($product){
        if ($this->CompareOutProduct($product)==True){
            echo 'Вы вытащили '.$product.' из холодильника '.$this->name;
        }
        else{
        	echo '<br>';
            echo 'Продукта '.$product.' нет в холодильнике '.$this->name.', пожалуйста, прочитайте этикетку и повторите попытку';
        }       
    }
}
class FridgeCamera extends Fridge{
    private $name;
    private $capacity = 'undef';
    private $products = [];
    private $fullness = '0';
    protected $type = 'Морозильная камера';
    public function clearFridgeCamera(){
        echo '<br>';
        echo 'Отлично! Вы убили всех микробов в вашей морозилке. Продуктам ничего не угрожает.';
    }
    
}
$fridge1 = new Fridge('LG K6000', '3');
$fridgeCamera1 = new FridgeCamera('FridgeCamera LG K6000', '2');
$fridgeCamera1 -> discription();
$fridgeCamera1 -> clearFridgeCamera();
$fridgeCamera1 -> putProduct('pepsi');
echo '<br>';

$fridge1 -> discription();
$fridge1 ->putProduct('banana');
$fridge1 ->putProduct('potato');
$fridge1 ->putProduct('milk');
$fridge1 ->putProduct('eggs');
$fridge1 -> pullOutProduct('milk');
$fridge1 -> pullOutProduct('pepsi');
$fridge1 ->putProduct('eggs');
$fridge1 -> discription();
?>
